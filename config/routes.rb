Rails.application.routes.draw do
  devise_for :users

  # root
  root "videos#show_video"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/', to: 'videos#show_video'

  resources :videos
  scope "/videos" do
    post "mark-video-as-marked", to: 'videos#mark_video_as_viewed_json'
    post "show-random-video", to: 'videos#show_random_video_json'
  end

  resources :genres
end
