class VideoUserViewed
  include Mongoid::Document

  field :watched, type: Boolean, default:false

  belongs_to :video
  belongs_to :user
end
