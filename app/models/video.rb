class Video
  include Mongoid::Document
  field :name, type: String
  field :youtube_id, type: String
  belongs_to :genre
end
