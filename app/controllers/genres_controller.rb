class GenresController < ApplicationController
  before_action :authenticate_user!
  def new
    if current_user.is_admin
      @genre = Genre.new

      render "new"
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  def create
    if current_user.is_admin
      @genre = Genre.new(genre_params)
      if @genre.save
        redirect_to genres_path
      else
        # This line overrides the default rendering behavior, which
        # would have been to render the "create" view.
        redirect_to new_genre_path, :alert => "Sorry, there is a problem"
      end
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  def update
    if current_user.is_admin
      @genre = Genre.find(params[:id])
      if @genre.update_attributes(genre_params)
        redirect_to genres_path
      else
        render 'edit'
      end
      render "new"
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end

  end

  def edit
    if current_user.is_admin
      @genre = Genre.find(params[:id])

      render "edit"
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  def destroy
    if current_user.is_admin
      @genre = Genre.find(params[:id])
      if @genre.destroy
        redirect_to videos_path, :alert => "Genre delete is successful"
      else
        redirect_to videos_path, :alert => "Sorry, there is a problem with deleting this genre"
      end
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  def index
    @genres = Genre.all

    render "index"
  end

  def show
    if current_user.is_admin
      @genre = Genre.find(params[:id])

      render "show"
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  private
    def genre_params
      params.require(:genre).permit(:name, :description)
    end
end
