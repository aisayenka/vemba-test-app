class VideosController < ApplicationController
  # authenticating user before running each method
  before_action :authenticate_user!
  def new
    # Showing new video from is the user is_admin,
    # otherwise, redirect the user to the root (Player) with the alert message
    if current_user.is_admin
      @video = Video.new

      render "new"
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  def create
    # Creating new video record from is the user is_admin,
    # otherwise, redirect the user to the root (Player) with the alert message
    if current_user.is_admin
      @video = Video.new(video_params)
      if @video.save
        redirect_to videos_path
      else
        # This line overrides the default rendering behavior, which
        # would have been to render the "create" view.
        redirect_to new_video_path, :alert => "Sorry, there is a problem"
      end
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  def update
    # Updating video record from is the user is_admin,
    # otherwise, redirect the user to the root (Player) with the alert message
    if current_user.is_admin
      @video = Video.find(params[:id])
      if @video.update_attributes(video_params)
        redirect_to videos_path
      else
        render 'edit'
      end
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end

  end

  def edit
    # Showing edit video from is the user is_admin,
    # otherwise, redirect the user to the root (Player) with the alert message
    if current_user.is_admin
      @video = Video.find(params[:id])

      render "new"
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  def destroy
    # Destroying video record from is the user is_admin,
    # otherwise, redirect the user to the root (Player) with the alert message
    if current_user.is_admin
      @video = Video.find(params[:id])
      if @video.destroy
        redirect_to videos_path, :alert => "Video delete is successful"
      else
        redirect_to videos_path, :alert => "Sorry, there is a problem with deleting this video"
      end
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  def index
    # Showing all video records from is the user is_admin,
    # otherwise, redirect the user to the root (Player) with the alert message
    if current_user.is_admin
      @videos = Video.all

      render "index"
    else
      redirect_to root_url, :alert => "Sorry, but you don't have the access to this page"
    end
  end

  def show_video
    # Preparing the Player page and getting the genres data before rendering
    @genres = Genre.all

    # getting the already watched videos
    # getting the ids
    video_user_watched_records_ids = get_watched_video_ids_by_the_user(current_user)

    # getting the videos
    @videos = Video.in(:id => video_user_watched_records_ids)

    # Genre id
    @genre_id = ""
    if(!session[:selected_genre_id].nil?)
      @genre_id = session[:selected_genre_id]
    end

    render "show_video"
  end

  def show_random_video_json
    # Setting up session variable
    session[:selected_genre_id] = params[:genre_id]

    # Getting random video which the user hasn't viewed
    result = show_random_video(params, current_user) # using a private method to get the random video

    # Responding in JSON format
    respond_to do |format|
      format.json  { render :json => result }
    end
  end

  def mark_video_as_viewed_json
    # Setting up session variable
    session[:selected_genre_id] = params[:genre_id]

    # Marking the viewed video as viewed for the user
    # And getting the new vide if any

    # Getting the video record
    video = Video.find(params[:video_id])

    # Getting the video/user record if any
    video_user_record = VideoUserViewed.where(:video => video).where(:user => current_user).first

    # If there is none specifying a new one. Also specifying the video and user reference
    if(video_user_record.blank?)
      video_user_record = VideoUserViewed.new
      video_user_record.user = current_user
      video_user_record.video = video
    end

    # Setting up the watched attribute to true, to exclude the video from the search
    video_user_record.watched = true

    # Check if the save is okay
    if video_user_record.save
      # If okay, get the random video using the private method
      result = show_random_video(params, current_user)

      # Specifying the previous video name
      result[:previous_video_name] = video.name

      # Responding in JSON format
      respond_to do |format|
        format.json  { render :json => result }
      end
    else
      # Respond that there is an error
      respond_to do |format|
        result = { :status => "error", :message => "Error!" }
        format.json  { render :json => result }
      end
    end
  end

  private
    def show_random_video(paramerters_in, user)
      # Getting the genre to load the video realtedto
      genre = Genre.find(paramerters_in[:genre_id])
      # Using a private method to load the video using the genre and user reference
      video = get_random_video(user, genre)

      # If no video specified returng appropriate message
      if video.nil?
        msg = {
          :status => "no-video-in-genre",
          :message => "No more videos in the genre"
        }
      # If everything is ok, specify the video id and the YOUTUBE video id
      else
        msg = {
          :status => "ok",
          :youtube_id => video.youtube_id,
          :video_id => video._id.to_s
        }
      end

      # return the appropariate message hash
      return msg
    end
    def get_random_video(user, genre)
      # getting the ids
      video_user_watched_records_ids = get_watched_video_ids_by_the_user(user)

      # getting the videos in the genre
      videos = Video.where(:genre => genre)
      # check if ids array is empty
      if(!video_user_watched_records_ids.blank?)
        # if not, the filter the records
        videos = videos.not_in(:id => video_user_watched_records_ids)
      end

      # if there is a next video (or videos), then the random one to watch
      if(!videos.blank?)
        random_number = rand(0..(videos.length - 1))
        return videos[random_number]
      else
        # if not, then return nil
        return nil
      end
    end

    def get_watched_video_ids_by_the_user(user)
      # Getting the records of already watched videos
      video_user_watched_records = VideoUserViewed.where(:user => user).where(:watched => true)
      # creating an array for the ids of the videos
      video_user_watched_records_ids = []
      #Setting up the previously watched videos
      video_user_watched_records.each { |e|
        video_user_watched_records_ids.push e.video_id
      }

      #returning the ids
      return video_user_watched_records_ids
    end

    # Parameters
    def video_params
      params.require(:video).permit(:name, :youtube_id, :genre)
    end
end
