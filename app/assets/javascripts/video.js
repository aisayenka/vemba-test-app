var videoPlayer = {

  init: function() {
    // Initial references to the elements
    videoPlayer.done = false;
    videoPlayer.genre_select_element = $('#genre-select');
    videoPlayer.authenticity_token_element = $("input[name=\"authenticity_token\"]");
    videoPlayer.next_video_button = $('#next-video');
    videoPlayer.video_id_holder = $('#video-id-holder');
    videoPlayer.youtube_video_id_holder = $('#youtube-video-id-holder');
    videoPlayer.no_video_message_holder = $('#video-error-message');
    videoPlayer.previously_watched_list = $('#previous-videos-list');

    // Hiding no video message element
    videoPlayer.no_video_message_holder.hide();

    // Pulling YouTube iframe API
    var tag = document.createElement('script'),
        body_tag = document.body;
    tag.src = "https://www.youtube.com/iframe_api";
    body_tag.appendChild(tag);


    // Setting up events for dropdown
    // Genre selection dropdown
    videoPlayer.genre_select_element.change(function() {
      videoPlayer.getRandomVideoOrMarkVideoAsViewed();
    });

    // Next button
    videoPlayer.next_video_button.click(function(e) {
      e.preventDefault(); // Just in case, to prevent submission of the form
      videoPlayer.getRandomVideoOrMarkVideoAsViewed();
    });
  },

  getRandomVideoOrMarkVideoAsViewed: function() {
    // Check if video_id is set up
    if (videoPlayer.video_id_holder.val() == "") {
      videoPlayer.get_random_video();
    } else {
      videoPlayer.mark_the_video_as_viewed();
    }
  },

  onPlayerReady: function(event) {
    // setting up the video
    videoPlayer.get_random_video();

    // Playing the video
    event.target.playVideo();
  },

  onPlayerStateChange: function(event) {
    // Check if the video play process is done
    if (event.data === 0) {
      // If so, just load a new video in the genre. If any
      videoPlayer.mark_the_video_as_viewed();
    }
  },

  update_video_in_player: function() {
    // Loading the new video
    videoPlayer.player.loadVideoById(videoPlayer.youtube_video_id_holder.val());
  },

  stopVideo: function() {
    // Stop the play process
    videoPlayer.player.stopVideo();

    // Marking the video
    videoPlayer.mark_the_video_as_viewed();
  },

  mark_the_video_as_viewed: function() {
    // Sending AJAX request to the server to mark the video as viewed
    // and get the new one in the process
    $.ajax({
      method: 'POST',
      url: '/videos/mark-video-as-marked.json',
      data: {
        video_id: videoPlayer.video_id_holder.val(),
        genre_id: videoPlayer.genre_select_element.find(':selected').attr('value'),
        authenticity_token: videoPlayer.authenticity_token_element.val()
      }
    }).done(function(data) {
      // If done successfully just setup the play process again
      videoPlayer.setup_video_and_youtube_ids(data);
    }).error(function(data) {
      // If there is an error, show the error message
      alert("Error occured:\n"+data.message);
    });
  },

  setup_video_and_youtube_ids: function(data) {
    // Check the status returned from the server
    switch (data.status) {
      default:
      case "ok":
        // if everithing is ok, just update the values, show all iframes
        // and update the video in the player
        $("iframe").show();
        videoPlayer.video_id_holder.val(data.video_id);
        videoPlayer.youtube_video_id_holder.val(data.youtube_id);
        videoPlayer.update_video_in_player();

        // Enabling the next video button
        videoPlayer.next_video_button.prop("disabled", false);

        break;
      case "no-video-in-genre":
        // If there is no video in the genre
        // Resetting the values of video holders
        videoPlayer.reset_video_information_holders();

        // Hide all iframes
        $("iframe").hide();

        // Disabling the next video button
        videoPlayer.next_video_button.prop("disabled", true);

        // Showing the message holder and the message about the problem
        videoPlayer.no_video_message_holder.show().text(data.message);

        break;
    }

    // add the previously watched video name to the list, if set
    if(data.previous_video_name !== undefined) {
      // Adding the last video_name to the list
      videoPlayer.previously_watched_list.prepend("<li>"+data.previous_video_name+"</li>");
    }
  },

  get_random_video: function() {
    // getting random video
    $.ajax({
      method: 'POST',
      url: '/videos/show-random-video.json',
      data: {
        genre_id: videoPlayer.genre_select_element.find(':selected').attr('value'),
        authenticity_token: videoPlayer.authenticity_token_element.val()
      }
    }).done(function(data) {
      // If done successfully just setup the play process again
      videoPlayer.setup_video_and_youtube_ids(data);
    }).error(function(data) {
      // If there is an error, show the error message
      alert("Error occured:\n"+data.message);
    });
  },

  reset_video_information_holders: function() {
    // Resetting the values
    videoPlayer.video_id_holder.val("");
    videoPlayer.youtube_video_id_holder.val("");
  }
};

// Function for YouTube iframe api to set the player up.
// Needed to be in the way, so there is no error from the API.
function onYouTubeIframeAPIReady() {
  var player, player_options, video_holder_element;
  video_holder_element = $('#video-player') || $(video_holder_element.selector);
  player_options = {
    height: '390',
    width: '640',
    videoId: video_holder_element.data("video-youtube-id"),
    events: {
      'onReady': videoPlayer.onPlayerReady,
      'onStateChange': videoPlayer.onPlayerStateChange
    }
  };
  videoPlayer.player = new YT.Player(video_holder_element.attr('id'), player_options);
  videoPlayer.get_random_video();
};

$("window").ready(function() {
  // When the page is loaded just initialize the whole script
  videoPlayer.init();
});
